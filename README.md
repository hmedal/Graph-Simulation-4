# Graph-Simulation-4

This project is to generate simulated graph from a given graph. This script has strong dependency on the name of the columns, types of the columns and format of the input file.

This script requires the following softwares:

1. Spark 2.1.0, prebuilt for Hadoop 2.7 and later

2. R 3.2.1 to generate graph properties.

3. Python 2.7.8 is required along with the following packages: mpi4py (using openmpi-1.10), numpy 1.10.4, panda 0.18.0, random, csv, gc (garbage collector), sklearn 0.18.1 (cluster, KMeans, kneighbors_graph, scale), sys, subprocess.

4. The code should be run with at least 4 processors. In this version, each processor is responsible for eah role. So, the minimum number of processors must be equal to the number of roles. (This condition will be removed from the next versions.)


# How to run Graph-Simulation-4

1.	Download "spark-2.1.0-bin-hadoop2.7.tgz" from http://spark.apache.org/downloads.html (choose Spark release 2.1.0 and package type: "Prebuilt for Apache Hadoop 2.7 and later") and unzip into your working directory (hereafter denoted as YOUR_WORKING_DIRECTORY). 
2.	Make all the files in {YOUR_WORKING_DIRECTORY}/spark-2.1.0-bin-hadoop2.7/bin" executable (e.g., using command chmod a+x bin).
3.	Download the Graph-Simulation-4 project and unzip it into YOUR_WORKING_DIRECTORY.
4.	Copy all the jar files from {YOUR_WORKING_DIRECTORY}/Graph-Simulation-4/Required_Packages to {YOUR_DIRECTORY}/spark-2.1.0-bin-hadoop2.7/jars.
5.	Copy the contents of {YOUR_WORKING_DIRECTORY}/Graph-Simulation-4 (except Required_Packages/ directory) in the working directory of your cluster.
6.	Keep all the input files (e.g. 11.binetflow, 5.binetflow and so on) in {YOUR_WORKING_DIRECTORY}/Graph-Simulation-4/input_files. These input files are used as seed graphs while generating large-scale simulated graph. All the input seed graphs must be inside the "input_files" directory, otherwise the input graphs will not be considered as input. (The user may also provide their own seed graphs.)
7.	Set the SPARK_HOME to the location of your spark installation (e.g., {YOUR_DIRECTORY}/spark-2.1.0-bin-hadoop2.7).
8.	Submit a job to the scheduler (we have provided an example script titled "runProject.pbs").
9.	When the job successfully terminates it will have generated the simulated graph in {YOUR_WORKING_DIRECTORY}/Graph-Simulation-4/SimulatedGraph.


# Results from Graph-Simulation-4

1. When Graph-Simulation-4 is run it will create a new folder named SimulatedGraph. The contents of this folder should be a series of small graphs beginning with "localgen_0.csv" and ending with "localgen_N.csv" where N is the number of local graphs generated - 1. This folder will also contain a file named upperlevelGraph.csv which contains the connections between local graphs which unite all local graphs into a single larger graph.
2. In order to verify that the simulation has completed and is correct compare the number of files in the SimulatedGraph folder with the nubmer of processors specified in the PBS script used to run the simulation. If the simulation is complete then all localgen_.csv files and upperlevelGraph.csv should be present and not empty.
